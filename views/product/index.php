


<div class="row">
	<div class="col-md-2">
		Назва:
	</div>
	<div class="col-md-8">
		<?php echo $product->name ?>
	</div>
</div>

<div class="row">
	<div class="col-md-2">
		image:
	</div>
	<div class="col-md-8">
		<img height=200" src="<?php echo $product->image ?>"/>
	</div>
</div>

<div class="row">
	<div class="col-md-2">
		Категорія:
	</div>
	<div class="col-md-8">

<?php if ($product->category->parent->parent) { ?>
		
			<?php echo $product->category->parent->parent->title ?>		
		/

<?php } ?>

<?php if ($product->category->parent) { ?>
		
			<?php echo $product->category->parent->title ?>		
		/

<?php } ?>

		<a href="/category/index?id=<?php echo $product->category->id ?>">
			<?php echo $product->category->title ?>		
		</a>
	</div>
</div>

<div class="row">
	<div class="col-md-2">
		Опис:
	</div>
	<div class="col-md-8">
		<?php echo $product->description ?>
	</div>
</div>

<div class="row">
	<div class="col-md-2">
		Ціна:
	</div>
	<div class="col-md-8">
	<?php echo $product->currency ?>  <?php echo $product->price ?>
	</div>
</div>
