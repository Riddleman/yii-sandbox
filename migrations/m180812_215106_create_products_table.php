<?php

use yii\db\Migration;

/**
 * Handles the creation of table `products`.
 */
class m180812_215106_create_products_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('products', [
            'id'            => $this->primaryKey(),
            'category_id'   => $this->string(255),
            'currency'      => $this->string(255),
            'price'         => $this->decimal(),
            'delivery'      => $this->boolean()->defaultValue(false),
            'description'   => $this->text()->defaultValue(null),
            'model'         => $this->string(255),
            'modified_time' => $this->timestamp(),
            'name'          => $this->string(255),
            'properties'    => $this->text()->defaultValue(null),
            'image'         => $this->text()->defaultValue(null),
            'original_id'   => $this->string(255),
            'group_id'      => $this->integer()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('products');
    }
}
