<?php 

    namespace app\models;

    use Yii;
    use yii\db\ActiveRecord;
    use yii\helpers\ArrayHelper;
    use app\models\Category;

    class Category extends ActiveRecord
    {
        /*
        public $id;
        public $parent_id;
        public $title;
        public $original_id;
*/
        public function rules() 
        {
            return [
                [['title'], 'required']
            ];
        }

        public static function tableName()
        {
            return 'categories';
        }

        public function getProducts()
        {
            return $this->hasMany(Product::className(), ['category_id' => 'original_id']);
        }


        public function getParent()
        {
            return $this->hasOne(Category::className(), ['original_id' => 'parent_id']);
        }

        public function multiInsert($categories) 
        {
            if (empty($categories) || !is_array($categories)) {
                return false;
            }

            $idList = array_column($categories, 'original_id');

            $exist = Category::find()
                ->select('original_id')
                ->where([
                    'original_id' => $idList
                ])
                ->all();

            $exist = array_column(ArrayHelper::toArray($exist), 'original_id');

            $prepared = [];
            foreach($categories as $item) {
                if (empty($item['title']) 
                    || empty($item['original_id'])
                    || in_array($item['original_id'], $exist) 
                    ) {
                    continue;
                }

                $prepared[] = [
                    'title'       => $item['title'],
                    'oroginal_id' => $item['original_id'],
                    'parent_id'   => empty($item['parent_id']) ? null : $item['parent_id']
                ];
            }

            if (empty($prepared)) {
                return false;
            }

            Yii::$app->db->createCommand()
                ->batchInsert(
                    $this->tableName(),
                    ['title', 'original_id', 'parent_id'],
                    $prepared
                )   
                ->execute();

            return true;
        }
    }
