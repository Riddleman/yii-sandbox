<?php 

    namespace app\models;

    use Yii;
    use yii\db\ActiveRecord;
    use yii\helpers\ArrayHelper;
    use app\models\Category;

    class Product extends ActiveRecord
    {
        /*
        public $id;
        public $parent_id;
        public $title;
        public $original_id;
*/
        public function rules() 
        {
            return [
                [['title'], 'required']
            ];
        }

        public static function tableName()
        {
            return 'products';
        }


        public function getCategory()
        {
             return $this->hasOne(Category::className(), ['original_id' => 'category_id']);
        }

        public function multiInsert($categories) 
        {

            if (empty($categories) || !is_array($categories)) {
                return false;
            }

            $idList = array_column($categories, 'original_id');

            $exist = self::find()
                ->select('original_id')
                ->where([
                    'original_id' => $idList
                ])
                ->all();




            $exist = array_column(ArrayHelper::toArray($exist), 'original_id');

            $prepared = [];
            foreach($categories as $item) {
                if (empty($item['name']) 
                    || empty($item['original_id'])
                    || empty($item['category_id'])
                    || in_array($item['original_id'], $exist) 
                    ) {
                    continue;
                }

                $prepared[] = [
                     'category_id'   => $item['category_id'],
                     'currency'      => empty($item['currency']) ? null : $item['currency'],
                     'price'         => empty($item['price']) ? null : intval($item['price']),
                     'delivery'      => empty($item['delivery']) ? null : $item['delivery'],
                     'description'   => empty($item['description']) ? null : $item['description'],
                     'model'         => empty($item['model']) ? null : $item['model'],
                     'modified_time' => empty($item['modified_time']) ? null : $item['modified_time'],
                     'name'          => empty($item['name']) ? null : $item['name'],
                     'properties'    => empty($item['properties']) ? null : $item['properties'],
                     'image'         => empty($item['image']) ? null : $item['image'],
                     'original_id'   => empty($item['original_id']) ? null : $item['original_id'],
                     'group_id'      => empty($item['group_id']) ? null : $item['group_id'],
                    
                ];
            }


            if (empty($prepared)) {
                return false;
            }

            Yii::$app->db->createCommand()
                ->batchInsert(
                    $this->tableName(),
                    [ 'category_id',  'currency', 'price', 'delivery', 'description', 'model', 'modified_time', 'name', 'properties', 'image', 'original_id', 'group_id'], 
                    $prepared
                )   
                ->execute();

            return true;
        }
    }
