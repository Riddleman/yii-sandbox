<?php

namespace app\commands;

use yii\console\Controller;
use yii\console\ExitCode;
use app\models\Category;
use app\models\Product;

class XmlController extends Controller
{
    // ...existing code...



    public function actionImport($fielName)
    {
        $this->actionCategories($fielName);
        $this->actionProducts($fielName);
    }


    public function actionCategories($fileName)
    {


        $sourceFile = __DIR__ . '/../xml_resources/' . $fileName;
        
        $xml = new \XMLReader();
        $xml->open($sourceFile);


        while($xml->read() && $xml->name != 'category') {
            ;
        }

        $categoryEntity = new Category();

        $processedNodes = 1;
        $categories = [];
        while($xml->name == 'category') {
            $node = new \SimpleXMLElement($xml->readOuterXML());

            
            $categories[] = [
                'original_id' => strval($node->attributes()->id),
                'parent_id'   => empty(strval($node->attributes()->parentId)) ? null : (strval($node->attributes()->parentId)),
                'title'       => strval($node)
            ];

            if ($processedNodes % 20 == 0) {
                $categoryEntity->multiInsert($categories);
                $categories = [];
            }

            $processedNodes++;
            
            $xml->next('category');
            unset($node);
        }
        

        if (!empty($categories)) {
            $categoryEntity->multiInsert($categories);  
        }

        $this->actionReport($processedNodes,'categories');
        $xml->close();

        //category
    }


    public function actionProducts($fileName)
    {
        $sourceFile = __DIR__ . '/../xml_resources/' . $fileName;
        
        $xml = new \XMLReader();
        $xml->open($sourceFile);


        while($xml->read() && $xml->name != 'offer') {
            ;
        }

        $productEntity = new Product();

        $processedNodes = 1;
        $products = [];
        while($xml->name == 'offer') {
            $node = new \SimpleXMLElement($xml->readOuterXML());
            
            //$this->actionProperties($xml);


            $properties = '[]';
            $delivery = empty(strval($node->delivery)) ? null : strval($node->delivery);
            $products[] = [
                'category_id'   => empty(strval($node->categoryId)) ? null : strval($node->categoryId),
                'currency'      => empty(strval($node->currencyId)) ? null : strval($node->currencyId),
                'delivery'      => (!empty($delivery) && $delivery == 'true'),
                'description'   => empty(strval($node->description)) ? null : strval($node->description),
                'price'         => empty(strval($node->price)) ? null : strval($node->price),
                'model'         => empty(strval($node->model)) ? null : strval($node->model), 
                'modified_time' => 0,//empty(strval($node->modified_time)) ? null : strval($node->modified_time), 
                'name'          => empty(strval($node->name)) ? null : strval($node->name), 
                'properties'    => $properties,
                'image'         => empty(strval($node->picture)) ? null : strval($node->picture), 
                'original_id'   => empty(strval($node->attributes()->id)) ? null : ($node->attributes()->id),
                'group_id'      => empty(strval($node->attributes()->group_id)) ? null : ($node->attributes()->group_id),
            ];

            if ($processedNodes % 20 == 0) {
                $productEntity->multiInsert($products);
                $products = [];
                print "processed $processedNodes\n";
            }

            // var_dump($categoryId);
            // die();
            
            // print_r($prod);
            // print "\n";
            $processedNodes++;
            
            $xml->next('offer');
            unset($node);

        }


        if (!empty($products)) {
            $productEntity->multiInsert($products);  
        }

        $this->actionReport($processedNodes,'products');
        $xml->close();
    }

    public function actionProperties($xml) {
        $iteratir = new \SimpleXMLIterator($xml);

        var_dump($iterator);
        die();
        while($current->read() && $current->name != 'param') {
            ;
        }

        while($xml->name == 'param') {
            $node = new \SimpleXMLElement($xml->readOuterXML());

            var_dump($node);
            die();
        }
    }

    public function actionReport($processedNodes, $nodeName = 'nodes') 
    {
        print "processed " . $nodeName . " =$processedNodes\n";
        print "memory_get_usage() =" . memory_get_usage()/1024 . "kb\n";
        print "memory_get_usage(true) =" . memory_get_usage(true)/1024 . "kb\n";
        print "memory_get_peak_usage() =" . memory_get_peak_usage()/1024 . "kb\n";
        print "memory_get_peak_usage(true) =" . memory_get_peak_usage(true)/1024 . "kb\n";
         
        //print "custom memory_get_process_usage() =" . memory_get_process_usage() . "kb\n";
 
 

    }
}
