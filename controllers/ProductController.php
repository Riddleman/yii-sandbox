<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\helpers\ArrayHelper;

use app\models\Product;
use yii\data\Pagination;

class ProductController extends Controller
{
    public function actionList($page = 0)
    {
        $query = Product::find();
	    $countQuery = clone $query;
	    $pages = new Pagination([
	    	'totalCount' => $countQuery->count(),
	    	'pageSize' => 20, 
	    	'page' => $page,
	    ]);

	    $products = $query->offset($pages->offset)
	        ->limit($pages->limit)
	        ->all();

	     
	    return $this->render('list', [
	         'products' => $products,
	         'pages'    => $pages,
	    ]);
    }

    public function actionIndex($id) 
    {

    	
    	$product = Product::findOne($id);


    	//var_dump($product->category);
    	//die();
    	
    	//$product = ArrayHelper::toArray($product);

	    return $this->render('index', [
	        'product' => $product
	    ]);
    }
}
