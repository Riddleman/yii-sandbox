<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\helpers\ArrayHelper;

use app\models\Category;
use app\models\Product;
use yii\data\Pagination;

class CategoryController extends Controller
{
    public function actionList($page = 0)
    {
        $query = Category::find();
	    $countQuery = clone $query;
	    $pages = new Pagination([
	    	'totalCount' => $countQuery->count(),
	    	'pageSize' => 20, 
	    	'page' => $page,
	    ]);

	    $categories = $query->offset($pages->offset)
	        ->limit($pages->limit)
	        ->all();
	       
	    return $this->render('list', [
	         'categories' => $categories,
	         'pages'    => $pages,
	    ]);
    }

    public function actionIndex($id, $page = 0)
    {
    	$category = Category::findOne($id);

    	
    	$query = Product::find()->where(['category_id' => $category->original_id]);
	    $countQuery = clone $query;
	    $pages = new Pagination([
	    	'totalCount' => $countQuery->count(),
	    	'pageSize' => 20, 
	    	'page' => $page,
	    ]);

	    $products = $query->offset($pages->offset)
	        ->limit($pages->limit)
	        ->all();

	    return $this->render('index', [
	        'category' => $category,
	        'pages' => $pages,
	        'products' => $products
	    ]);
    }
}
